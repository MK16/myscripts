//
// Created by Martin Kocisky on 05/08/2016.
//

#include <vector>

using namespace std;

vector<int> merge(vector<int> &v1, vector<int> &v2) {
    if (v1.empty()) return v2;
    if (v2.empty()) return v1;

    vector<int> result;
    int v1_index = 0;
    int v2_index = 0;

    while (true) {
        if (v1_index < v1.size() && (v1[v1_index] <= v2[v2_index] || v2_index == v2.size())) {
            result.push_back(v1[v1_index]);
            v1_index++;
            continue;
        }
        if (v2_index < v2.size()) {
            result.push_back(v2[v2_index]);
            v2_index++;
            continue;
        }
        break;
    }
    return result;
}
/**
 *
 * @param v vector of integers to be sorted
 * @param begin index of the beginning 0
 * @param end index of end (not last element), (closed - open), therefore size
 * @return new sorted array
 */
vector<int> merge_sort(vector<int> &v, int begin, int end) {
    if (begin == end) return vector<int> {};
    if (begin + 1 == end) return vector<int> {v[begin]};
    int middle = (begin + end) / 2;

    vector<int> v1 = merge_sort(v, begin, middle);
    vector<int> v2 = merge_sort(v, middle, end);

    return merge(v1, v2);
}
