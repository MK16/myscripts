#include <vector>

using namespace std;

void swap(int &a, int &b) {
    int tmp = b;
    b = a;
    a = tmp;
}

void heapify(vector<int> &v, int index, int size) {
    int left_index = index * 2 + 1;
    int right_index = index * 2 + 2;

    if (left_index < size && right_index < size) {
        if (v[left_index] > v[right_index] && v[index] < v[left_index]) {
            swap(v[left_index], v[index]);
            heapify(v, left_index, size);
        } else if (v[index] < v[right_index]) {
            swap(v[right_index], v[index]);
            heapify(v, right_index, size);
        }
    } else if (left_index < size && v[index] < v[left_index]) {
        swap(v[left_index], v[index]);
        heapify(v, left_index, size);
    } else if (right_index < size && v[index] < v[right_index]) {
        swap(v[right_index], v[index]);
        heapify(v, right_index, size);
    }
}

/**
 * Make a heap in a vector
 * @param v
 */
void make_heap(vector<int> &v) {
    for (int i = v.size() / 2; i >= 0; i--) {
        heapify(v, i, v.size());
    }
}

/**
 * Heap sort a vector
 * @param v
 */
void heap_sort(vector<int> &v) {
    make_heap(v);
    for (int i = v.size() - 1; i != 0; i--) {
        swap(v[0], v[i]);
        heapify(v, 0, i);
    }
}