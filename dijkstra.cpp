//
//  dijkstra.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 21/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <climits>
#include <queue>
#include <map>
using namespace std;

// pair<vertex_2, path_value>
auto c = [](pair<int, int> p1, pair<int, int> p2) { return p1.second < p2.second; };
vector<priority_queue<pair<int, int>, vector<pair<int, int>, decltype(c)>>(c)> adjacency_list {
    {},
    {},
    {},
    {},
};

class vertex {
public:
    vertex(int key): key(key) {};
    int key;
    vertex *previous;
    int path = INT_MAX;
};

int dijkstra(vector<vertex> vertices, vector<vector<pair<int, int>>> adjacency_list, int start, int end) {
    auto cmp = [](vertex v1, vertex v2) { return v1.path > v2.path; };
    priority_queue<vertex, vector<vertex>, decltype(cmp)> q(cmp);

    q.push(vertices[start]);

    while (!q.empty()) {
        vertex v = q.top();
        q.pop();

        for (auto &p : adjacency_list[v.key]) {
            if (vertices[p.first].path > (v.path + p.second)) {
                vertices[p.first].path = v.path + p.second;
                vertices[p.first].previous = &v;
                q.push(vertices[p.first]);
            }
        }
    }
    return vertices[end].path;
}

