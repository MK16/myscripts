//
//  main.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 12/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <iostream>
#include <vector>
#include <exception>

using namespace std;

class not_found : public std::exception {
    const char* what() const noexcept {return "Not found!\n";}
};

template<typename T>
class binary_node {
    int balance_factor = 0;
    int height = 0;

    int key;
    T value;

    binary_node *left = nullptr;
    binary_node *right = nullptr;

    template<typename>friend class avl_binary_tree;
public:
    binary_node(int key, T &value) : key(key),value(value) {

    }

    ~binary_node() {
        if (left) {
            delete left;
        }
        if (right) {
            delete right;
        }
    }
};

template<typename T>
class avl_binary_tree {
    binary_node<T> * root = nullptr;

    binary_node<T>* binarySearch(binary_node<T> *node, int key) {
        if (node == nullptr) {
            throw new not_found;
        }
        if (node->key == key) {
            return node;
        } else if (node->key > key) {
            return binarySearch(node->left, key);
        } else {
            return binarySearch(node->right, key);
        }
    }

    int height(binary_node<T> *node) {
        if (node == nullptr) {
            return 0;
        }
        return 1 + (height(node->left) > height(node->right) ? height(node->left) : height(node->right));
    }

    binary_node<T>* rotateLeft(binary_node<T> *node) {
        binary_node<T> *new_root = node->right;
        node->right = node->right->left;
        if (node->right != nullptr) node->right->left = node;
        new_root->left = node;
        node->height = height(node);
        new_root->height = height(new_root);
        return new_root;
    }

    binary_node<T>* rotateRight(binary_node<T> *node) {
        binary_node<T> *new_root = node->left;
        node->left = node->left->right;
        if (node->left != nullptr) node->left->right = node;
        new_root->right = node;
        node->height = height(node);
        new_root->height = height(new_root);
        return new_root;
    }

    binary_node<T>* insert_node(binary_node<T> *node, int key, T &value) {
        if (node == nullptr) return new binary_node<T>(key, value);

        if (node->key > key) {
            node->left = insert_node(node->left, key, value);
        } else {
            node->right = insert_node(node->right, key, value);
        }

        node->height = height(node);
        node->balance_factor = -(node->left == nullptr ? 0 : node->left->height) +
                                (node->right == nullptr ? 0 : node->right->height);

        // insert rotation here for balancing

        // Left Left
        if (node->balance_factor < -1 && key < node->left->key) {
            return rotateRight(node);
        }
        // Left Right
        if (node->balance_factor < -1 && key > node->left->key) {
            node->left = rotateLeft(node->left);
            return rotateRight(node);
        }
        // Right Right
        if (node->balance_factor > 1 && key > node->right->key) {
            return rotateLeft(node);
        }
        // Right Left
        if (node->balance_factor > 1 && key < node->right->key) {
            node->right = rotateRight(node->right);
            return rotateLeft(node);
        }
        return node;
    }

    void printInOrder(binary_node<T> *root) {
        if (root == nullptr) {
            return;
        }
        cout << '(';
        if (root->left != nullptr) printInOrder(root->left);
        cout << ']' << root->key << '[';
        if (root->right != nullptr) printInOrder(root->right);
        cout << ')';
    }

public:
    T search(int key) {
        return binarySearch(root, key)->value;
    }

    void insert(int key, T &value) {
        root = insert_node(root, key, value);
    }

    void remove(int key) {
        binary_node<T> *node = binarySearch(root, key);
        if (node->parent.right == node) {
            node->parent.right = nullptr;
        } else {
            node->parent.left = nullptr;
        }
        delete node;
    }
    
    void print() {
        printInOrder(root);
    }
};

int main(int argc, const char * argv[]) {
    avl_binary_tree<int> t;
    int a = 13;
    t.insert(1, a);
    t.insert(2, a);
    t.insert(3, a);
    t.insert(4, a);
    t.insert(5, a);
    t.insert(6, a);
    t.print();
    return 0;
}
