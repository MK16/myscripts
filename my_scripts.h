//
// Created by Martin Kocisky on 06/08/2016.
//

#ifndef MYSCRIPTS_MY_SCRIPTS_H
#define MYSCRIPTS_MY_SCRIPTS_H

#include <vector>

using namespace std;

vector<int> merge_sort(vector<int> &v, int begin, int end);
vector<int> merge(vector<int> v1, vector<int> v2);
void quick_sort(vector<int> &v, int begin, int end);

void swap(int &a, int &b);
void heapify(vector<int> &v, int index, int size);
void make_heap(vector<int> &v);
void heap_sort(vector<int> &v);

#endif //MYSCRIPTS_MY_SCRIPTS_H
