#include <vector>
#include <exception>

using namespace std;

class not_found : public exception {
    const char* what() const noexcept {return "Not found!\n";}
};

int binary_search(vector<int> &v, int val, int b, int e) {
    if (b == e) throw new not_found;
    if ((b + e) / 2 < e && v[(b + e) / 2] == val) return v[v.size() / 2];
    if ((b + e) / 2 < e && v[(b + e) / 2] > val) {
        return binary_search(v, b, (b + e) / 2, val);
    } else {
        return binary_search(v, (b + e) / 2, e, val);
    }
}
