//
//  problems.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 20/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <string>
#include <climits>
#include <list>

using namespace std;

// Given a matrix of dimensions mxn having all entries as 1 or 0, find out the size of maximum size square sub-matrix with all 1s.

int min3(int a, int b, int c) {
    if (a < b && a < c) {
        return a;
    } else if (b < a && b < c) {
        return b;
    } else {
        return c;
    }
}

int maxSquareSubMatrix_1(vector<vector<unsigned short>> matrix) {
    vector<vector<unsigned short>> result(matrix.size(), vector<unsigned short>(matrix[0].size()));

    for (int i = 0; i < matrix.size(); i++) {
        for (int j = 0; j < matrix[i].size(); j++) {
            if (i == 0 || j == 0) {
                result[i][j] = matrix[i][j];
            } else {
                if (matrix[i][j] == 0) {
                    result[i][j] = 0;
                } else {
                    result[i][j] = 1;
                    result[i][j] += min3(matrix[i - 1][j], matrix[i][j - 1], matrix[i - 1][j - 1]);
                }
            }
        }
    }

    int max = 0;
    for (const auto &v : result) {
        for (const auto i : v) {
            if (i > max) {
                max = i;
            }
        }
    }
    return max;
}

// Given two strings S1 and S2. Find the longest common substring between S1 and S2.

int longestCommonSubstring(string s1, string s2) {
    int max = 0;
    for (int i = 0; i < s1.size(); i++) {
        int t_max = 0;
        if (max > s1.size() - i) {
            break;
        }
        for (int j = 0; j < s2.size(); j++) {
            int t2_max = 0;
            if (max > s2.size() - j) {
                break;
            }
            for (int k = 0; k < s2.size(); k++) {
                if (s1[i + k] == s2[j + k]) {
                    t2_max++;
                } else {
                    break;
                }
            }
            if (t2_max > t_max) {
                t_max = t2_max;
            }
        }
        if (t_max > max) {
            max = t_max;
        }
    }
    return max;
}

// NOT THIS ONE

//int longestCommonSubsequence(string s1, string s2) {
//    vector<vector<unsigned short>> result(s1.size() + 1, vector<unsigned short>(s2.size() + 1, 0));
//
//    for (int i = 1; i <= s1.size(); i++) {
//        for (int j = 1; j <= s2.size(); j++) {
//            result[i][j] = result[i - 1][j];
//            if (s1[i] == s2[j]) {
//                result[i][j] += 1;
//            }
//        }
//    }
//    return result[result.size() - 1][result[0].size() - 1];
//}

// Given an array of integers, find a contiguous subsequence with the highest sum of elements. For example, in the array {1, -2, 3, 10, -4, 7, 2, -5}, its sub-array {3, 10, -4, 7, 2} has the maximum sum 18.

vector<int> continuousSubsequenceMaxSum(vector<int> numbers) {
    if (numbers.size() == 1) {
        return numbers;
    }

    list<tuple<int, int, int>> sums;
    int sum = numbers[0];
    int b = 0;
    int e = 0;

    for (int i = 1; i < numbers.size(); i++) {
        if (numbers[i] >= 0) {
            if (sum >= 0) {
                sum += numbers[i];
                e++;
            } else {
                sums.push_back(tuple<int, int, int>(sum, b, e));
                sum = numbers[i];
                b = i;
            }
            sum += numbers[i];
            e++;
        } else if (numbers[i] < 0) {
            if (sum < 0) {
                sum += numbers[i];
                e++;
            } else {
                sums.push_back(tuple<int, int, int>(sum, b, e));
                sum = numbers[i];
                b = i;
            }
        }
    }
    for (auto x : sums) {
        cout << get<0>(x) << get<1>(x) << get<2>(x);
    }
    return numbers;
}

//vector<int> continuousSubsequenceMaxSum(vector<int> v) {
//    vector<vector<int>> result;
//    for (int i = 0; i < v.size(); i++) {
//        vector<int> row;
//        for (int j = i; j < v.size(); j++) {
//            if (i == j) {
//                row.push_back(v[j]);
//            } else {
//                row.push_back(*row.rbegin() + v[j]);
//            }
//        }
//        result.push_back(row);
//    }
//
//    int max = INT_MIN;
//    int x = 0;
//    int y = 0;
//    for (int i = 0; i < result.size(); i++) {
//        for (int j = 0; j < result[i].size(); j++) {
//            if (result[i][j] > max) {
//                max = result[i][j];
//                x = i;
//                y = j;
//            }
//        }
//    }
//    vector<int> out;
//    for (auto i = v.begin() + x; i != v.begin() + x + y + 1; i++) {
//        out.push_back(*i);
//    }
//    return out;
//}
