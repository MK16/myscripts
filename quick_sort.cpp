//
// Created by Martin Kocisky on 06/08/2016.
//

#include <vector>

using namespace std;

void quick_sort(vector<int> &v, int begin, int end) {
    // [begin, end)
    if (begin + 1 >= end) return;
    int pivot = v[(begin + end) / 2];
    int b = begin;
    int e = end - 1;

    // at the end b == e [begin...<pivot>,b/e...end)
    while (b < e) {
        while (v[b] <= pivot && b < e) {
            b++;
        }
        while (v[e] > pivot && b < e) {
            e--;
        }
        if (b < e) {
            swap(v[b], v[e]);
            b++;
            e--;
        }
    }

    quick_sort(v, begin, b);
    quick_sort(v, b, end);
}
    
