//
//  breadth_first_search.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 14/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include "my_scripts.h"

class position {
public:
    int x;
    int y;
    int length;
    position *predeccessor;
};

void checkField(queue<position *> &q, vector<vector<char>> &a, position *current, int pos_x, int pos_y) {
    if (pos_x >= 0 && pos_x < a.size() &&
        pos_y >= 0 && pos_y < a[0].size()&&
        a[pos_x][pos_y] == 'X') {
        position *p = new position {.x = pos_x, .y = pos_y, .length = current->length + 1, .predeccessor = current};
        q.push(p);
    }
}
position* BFS(vector<vector<char>> &a, int s_x, int s_y) {
    queue<position *> q;
    position *p = new position{.x = s_x, .y = s_y, .length = 0, .predeccessor = nullptr};
    q.push(p);

    while (!q.empty()) {
        p = q.front();
        q.pop();

        if (a[p->x][p->y] == 'E') {
            cout << "Done! Length: " << p->length << endl;
            break;
        }
        
        checkField(q, a, p, p->x + 1, p->y);
        checkField(q, a, p, p->x - 1, p->y);
        checkField(q, a, p, p->x, p->y + 1);
        checkField(q, a, p, p->x, p->y - 1);
            
        a[p->x][p->y] = '*';
    }
    return p;
}
