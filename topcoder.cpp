//
//  topcoder.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 14/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cctype>
#include <queue>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <cmath>
#include <algorithm>
using namespace std;

class PartySeats {
public:
    static vector<string> seating(vector<string> attendees) {
        multiset<string> boys;
        multiset<string> girls;

        for (const auto &s : attendees) {
            stringstream ss(s);
            string name;
            string gender;
            ss >> name >> gender;
            if (gender == "boy")
                boys.insert(name);
            else
                girls.insert(name);
        }
        if (boys.size() < 2 || girls.size() < 2 ||
            girls.size() != boys.size())
        {
            return vector<string> {};
        }
        vector<string> result {"HOST"};
        int flag = 1;
        while(!girls.empty()) {
            if (girls.size() == (attendees.size() / 4) && flag-- == 1) {
                result.push_back("HOSTESS");
            } else {
                result.push_back(*girls.begin());
                girls.erase(girls.begin());
            }
            if (!boys.empty()) {
                result.push_back(*boys.begin());
                boys.erase(boys.begin());
            }
        }
        return result;
    }
};

class PaperFold {
    void get(vector<int> &v, double &min, double &max) {
        if (v[0] > v[1]) {
            min = v[1];
            max = v[0];
        } else {
            min = v[0];
            max = v[1];
        }
    }
public:
    int numFolds(vector<int> paper, vector<int> box) {
        double min, max;
        get(paper, min, max);
        double b_min, b_max;
        get(box, b_min, b_max);
        int fold = 0;
        while((max > b_max || min > b_min) && fold <=8) {
            if (max > b_max) {
                max /= 2;
                fold++;
            } else {
                min /=2;
                fold++;
            }
            if (max < min) swap(min, max);
        }
        if (fold > 8) return -1;
        return fold;
    }
};

class RegularSeason {
    typedef struct entry {
        string name;
        vector<int> p;
    } entry;
    vector<entry> parseTeams(vector<string> &teams) {
        vector<entry> r;
        for (auto &s : teams) {
            entry e;
            stringstream ss(s);
            ss >> e.name;
            while (ss.good()) {
                int i;
                ss >> i;
                e.p.push_back(i);
            }
            r.push_back(e);
        }
        return r;
    }

    bool cmp(pair<string, double> p1, pair<string, double> p2) {
        if (p1.second > p2.second)
            return true;
        else if (p2.second > p1.second)
            return false;
        else {
            return p1.first > p2.first;
        }
    }
public:
    vector<string> finalStandings(vector<string> teams, int round) {
        vector<entry> v = parseTeams(teams);
        map<string, double> m;
        for (int i = 0; i < v.size(); i++) {
            entry &e = v[i];
            if (m.find(e.name) == m.end()) m.emplace(e.name, 0);
            for(int j = 0; j < e.p.size(); j++) {
                if (i == j) continue;
                m[e.name] += (e.p[j] / 100.0) * round;
                if (m.find(v[j].name) == m.end()) m.emplace(v[j].name, 0);
                m[v[j].name] += ((100 - e.p[j]) / 100.0) * round;
            }
        }
        vector<pair<string, double>> r;
        for (auto &e : m) {
            r.push_back(pair<string, double>(e.first, e.second));
        }
        sort(r.begin(), r.end(), cmp);
        vector<string> result;
        for (auto &e : r) {
            stringstream ss;
            ss << e.first << " " << std::round(e.second);
            result.push_back(ss.str());
        }
        return result;
    }
};

// 207 2
#include <vector>
#include <map>
#include <string>
#include <sstream>
using namespace std;

class RegularSeason {
    typedef struct entry {
        string name;
        vector<int> p;
    } entry;
    vector<entry> parseTeams(vector<string> &teams) {
        vector<entry> r;
        for (auto &s : teams) {
            entry e;
            stringstream ss(s);
            ss >> e.name;
            while (!ss.empty()) {
                int i;
                ss >> i;
                e.p.push_back(i);
            }
            r.push_back(e);
        }
        return r;
    }

public:
    vector<string> finalStandings(vector<string> teams, int round) {
        vector<entry> v = parseTeams(teams);
        map<string, double> m;
        for (int i = 0; i < v.size(); i++) {
            entry &e = v[i];
            if (m.find(e.name) == m.end()) m.emplace(e.name, 0);
            for(int j = 0; j < e.p.size(); j++) {
                if (i == j) continue;
                m[e.name] += (e.p[j] / 100.0) * round;
            }
        }
        vector<string> result;
        for (auto &e : m) {
            stringstream ss;
            ss << m.first << " " << m.second;
            result.push_back(ss.str());
        }
        return result;
    }
};

class WordCompositionGame {
public:
    string score(vector <string> listA, vector <string> listB, vector <string> listC) {
        multimap<string, char> m;
        set<string> keys;
        for (const auto &s : listA) {
            m.emplace(s, 'A');
            keys.insert(s);
        }
        for (const auto &s : listB) {
            m.emplace(s, 'B');
            keys.insert(s);
        }
        for (const auto &s : listC) {
            m.emplace(s, 'C');
            keys.insert(s);
        }
        int a = 0;
        int b = 0;
        int c = 0;
        for (const auto &s : keys) {
            int cc = m.count(s);
            for (auto i = m.equal_range(s).first; i != m.equal_range(s).second; i++) {
                if (i->second == 'A') a += (cc == 1 ? 3 : (cc == 2 ? 2 : 1));
                if (i->second == 'B') b += (cc == 1 ? 3 : (cc == 2 ? 2 : 1));
                if (i->second == 'C') c += (cc == 1 ? 3 : (cc == 2 ? 2 : 1));
            }
        }
        stringstream ss;
        ss << a << "/" << b << "/" << c;
        return ss.str();
    }
};

class InsideOut {
public:
    string unscramble(string line) {
        string h1 = line.substr(0, line.length() / 2);
        string h2 = line.substr(line.length() / 2, line.length() / 2);
        stringstream ss;
        for(auto i = h1.rbegin(); i != h1.rend(); i++)
            ss << *i;
        for(auto i = h2.rbegin(); i != h2.rend(); i++)
            ss << *i;
        return ss.str();
    }
};

class SentenceSplitting {
public:
    static int split(string sentence, int k) {
        vector<int> words;
        stringstream ss(sentence);
        while (ss.good()) {
            string s;
            ss >> s;
            words.push_back(s.length());
        }
        //	int spaces = words.size() - 1;
        int p_l = sentence.length() / (k + 1);
        vector<int> parts_length(k + 1);

        int index = 0;
        for (int i = 0; i <= k; i++) {
            int length = 0;
            if (i == k) {
                while (index < words.size()) {
                    if (length != 0) length++;
                    length += words[index];
                    index++;
                }
                parts_length[i] = length;
                break;
            }

            while (length < p_l) {
                if (index == words.size()) break;
                if (length != 0) length++;
                length += words[index];
                index++;
            }
            if (index != words.size())
                if (abs(p_l - length) > abs(p_l - (length + 1 + words[index]))) {
                    if (length != 0) length++;
                    length += words[index];
                    index++;
                }
            parts_length[i] = length;
        }
        sort(parts_length.begin(), parts_length.end());
        return parts_length[parts_length.size() - 1];
    }
};

// SRM224 DIV 2 500
class TwoTurtledoves {
public:
    int presentType(int n) {
        int day = 1;
        int presents = 1;
        int p = 1;
        while (presents < n) {
            day++;
            p = p + day;
            presents += p;
        }
        int type = 1;
        int count = 0;
        while (presents != n) {
            presents--;
            count++;
            if (count == type) {
                type++;
                count = 0;
            }
        }
        return type;
    }	
};

//SRM 325 DIV 2 500 NON FUNCTIONAL, see revision_2016 doc
class RGBStreet {
public:
    static int estimateCost(vector<string> houses) {
        int total = INT_MAX;
        for (int i = 0; i < 3; i++) {
            for (int j = 1; j <= 2; j++) {
                int sum = 0;
                for (int k = 0; k < houses.size(); k++) {
                    stringstream ss;
                    ss << houses[k];
                    int r,g,b;
                    ss >> r >> g >> b;
                    vector<int> v {r,g,b};
                    if (k % 3 == 0)
                        sum += v[i];
                    else if (k % 3 == 1)
                        sum += v[(i + j) % 3];
                    else {
                        sum += v[(6 - ((i + j) % 3) - i) % 3];
                    }
                }
                if (sum < total) total = sum;
            }
        }
        return total;
    }
};

class SalaryCalculator {
public:
    double calcHours(int p1, int p2, int salary) {
        double d = static_cast<double>(salary) / p1;
        if (d > 200) {
            salary -= p1 * 200;
            return 200 + static_cast<double>(salary) / p2;
        } else {
            return d;
        } 
    }
};

class DietPlan {
public:
    string chooseDinner(string diet, string breakfast, string lunch) {
        stringstream ss;
        for (auto &c : diet) {
            int pos = breakfast.find(c);
            int poss = lunch.find(c);

            if (pos != string::npos) {
                if (poss == string::npos) {
                    breakfast[pos] = '.';
                    continue;
                } else {
                    return "CHEATER";
                }
            } else if (poss != string::npos) {
                lunch[poss] = '.';
                continue;
            }
            ss << c;
        }

        for (auto &c : breakfast) if (c != '.') return "CHEATER";
        for (auto &c : lunch) if (c != '.') return "CHEATER";
        
        string r = ss.str();
        sort(r.begin(), r.end());
        return r;
    }
};

class EggCartons {
public:
    static int minCartons(int n) {
        priority_queue<int, vector<int>, greater<int>> q;
        for (int i = 0; i <= n / 8; i++) {
            for (int j = 0; j <= n / 6; j++) {
                if (((i * 8) + (j * 6)) == n) {
                    q.push(i + j);
                }
            }
        }
        if (q.empty()) return -1;
        return q.top();
    }
};

class KDoubleSubstrings {
private:
    static bool diff(string &s, int b, int e, int k) {
        int df = 0;
        for(int i = 0; i < (e - b) / 2; i++) {
            if (s[b + i] != s[b + (e - b) / 2 + i]) df++;
        }
        return (df <= k ? true : false);
    }

public:
    static int howMuch(vector<string> str, int k) {
        int total = 0;
        stringstream ss;
        for(auto &s : str) {
            ss << s;
        }
        string m = ss.str();
        for(int i = 2; i <= m.length(); i+= 2) {//length of substring
            int p = 0;//position of start of substring
            while(p + i - 1 < m.length()) {//chceck all substring from start to end
                if (diff(m, p, p + i, k)) total++;
                    p++;
            }
        }
        
        return total;
    }
};

class NamingConvention {
public:
    static string toCamelCase(string variableName) {
        vector<string> parts;
        int start = 0;
        while(start < variableName.length()) {
            int end = start;
            while(end <= variableName.length()) {
                if(variableName.length() == end || variableName[end] == '_') {
                    string s = variableName.substr(start, end - start);
                    if(start != 0) s[0] = toupper(s[0]);
                    parts.push_back(s);
                    break;
                }
                end++;
            }
            start = end + 1;
        }
        stringstream ss;
        for(auto &s : parts) {
            ss << s;
        }
        return ss.str();
    }
};
