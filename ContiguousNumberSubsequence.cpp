//
//  ContiguousNumberSubsequence.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 24/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <stdio.h>
#include <vector>
using namespace std;

vector<int> continuousSubsequenceMaxSum(vector<int> numbers) {
    if (numbers.size() == 1) {
        return numbers;
    }

    // sum, [begining, end)
    vector<tuple<int, int, int>> sums;
    int sum = numbers[0];
    int b = 0;
    int e = 1;

    for (int i = 1; i < numbers.size(); i++) {
        if (numbers[i] >= 0) {
            if (sum >= 0) {
                sum += numbers[i];
                e++;
            } else {
                sums.push_back(tuple<int, int, int>(sum, b, e));
                sum = numbers[i];
                b = i;
                e = i + 1;
            }
        } else if (numbers[i] < 0) {
            if (sum < 0) {
                sum += numbers[i];
                e++;
            } else {
                sums.push_back(tuple<int, int, int>(sum, b, e));
                sum = numbers[i];
                b = i;
                e = i + 1;
            }
        }
    }
    sums.push_back(tuple<int, int, int>(sum, b, e));

    int max_l = 0;
    int max_r = 0;
    int start_l = 0;
    int start_r = 0;
    for (int i = 0; i < sums.size(); i++) {
        if ((max_r + get<0>(sums[i])) >= max_r) {
            max_r += get<0>(sums[i]);
        } else {
            if ((i + 1) < sums.size() && max_r + get<0>(sums[i]) >= 0) {
                max_r += get<0>(sums[i]);
                continue;
            }
            max_l = max_r;
            max_r = 0;
            start_l = start_r;
            start_r = i + 1;
        }
    }
    int total_max = 0;
    int start = 0;
    if (max_r > max_l) {
        total_max = max_r;
        start = start_r;
    } else {
        total_max = max_l;
        start = start_l;
    }
    vector<int> result;
    for (int i = get<1>(sums[start]); total_max != 0 ; i++) {
        result.push_back(numbers[i]);
        total_max -= numbers[i];
    }
    return numbers;
}
