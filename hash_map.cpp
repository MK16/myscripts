//
//  hash_map.cpp
//  MyScriptsXcode
//
//  Created by Martin Kocisky on 14/09/2016.
//  Copyright © 2016 Martin Kocisky. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <list>
using namespace std;

template<typename T>
class hash_map {
    int size;
    vector<list<pair<int, T>>> *data;
public:
    hash_map(int size) : size(size) {
        data = new vector<list<pair<int, T>>>(size);
    }

    ~hash_map() {
        delete data;
    }

    void insert(int key, T value) {
        (*data)[hash(key)].push_back(new pair<int, T>(key, value));
    }

    void remove(int key, T value) {
        for(auto i = (*data)[hash(key)].begin(); i != (*data)[hash(key)].end(); i++) {
            if (*i.first() == key) (*data)[hash(key)].remove(i);
        }
    }

    bool find(int key) {
        for(auto i = (*data)[hash(key)].begin(); i != (*data)[hash(key)].end(); i++) {
            if (*i.first() == key) return true;
        }
        return false;
    }

private:
    int hash(int key) {
        return (key * 13) % size;
    }
};
